WEBVTT

1
00:00:05.370 --> 00:00:07.680
Unknown: Let's talk about
debugging Django applications in

2
00:00:07.680 --> 00:00:11.070
VS code. This is particularly
useful if your application

3
00:00:11.100 --> 00:00:13.800
doesn't produce the right
result. And you want to run it

4
00:00:13.800 --> 00:00:16.590
line by line and see where
exactly something has gone

5
00:00:16.590 --> 00:00:21.060
wrong. So click on this icon to
open the run and debug panel.

6
00:00:21.810 --> 00:00:24.960
Now the first time you see this
message saying, we should create

7
00:00:25.140 --> 00:00:28.980
a large JSON file, basically, we
need to create a large profile.

8
00:00:29.130 --> 00:00:33.360
So VS code knows how to run or
debug this application. So let's

9
00:00:33.360 --> 00:00:39.270
click on this. Now, in this
last, select Django. Alright,

10
00:00:39.330 --> 00:00:42.390
here's our launch book JSON
file. What you see here is kind

11
00:00:42.390 --> 00:00:45.450
of like a dictionary in Python,
you have a bunch of key value

12
00:00:45.450 --> 00:00:50.220
pairs, these key value pairs
define a launch profile. So VS

13
00:00:50.220 --> 00:00:54.120
code knows that to run this
application, it should use the

14
00:00:54.120 --> 00:00:59.460
current Python interpreter to
run manage.py, which exists in

15
00:00:59.460 --> 00:01:03.420
our workspace folder. And then
as an argument, it should pass

16
00:01:03.510 --> 00:01:08.610
run server to it. Here, we can
add an additional argument to

17
00:01:08.610 --> 00:01:12.510
specify the port so it doesn't
clash with Port 8000, which is

18
00:01:12.510 --> 00:01:16.440
currently in use. Okay, save the
changes, you're done with this

19
00:01:16.440 --> 00:01:21.030
file. So let's close it. Now, if
you're curious, let me show you

20
00:01:21.030 --> 00:01:26.490
where this file exists, is added
to our project up here inside

21
00:01:26.700 --> 00:01:30.780
the VS code folder. So here's
our launch profile. Now that we

22
00:01:30.780 --> 00:01:34.800
have a launch profile, if we go
to the run and debug panel, we

23
00:01:34.800 --> 00:01:37.710
see something else. so here we
can start our application for

24
00:01:37.710 --> 00:01:41.220
debugging. But before doing so,
I want to write some dummy code

25
00:01:41.250 --> 00:01:46.710
here in our view function. So
let's set x to one and y to two.

26
00:01:47.760 --> 00:01:51.330
Now, I'm going to click on this
line to insert a breakpoint.

27
00:01:51.870 --> 00:01:55.590
When we add a breakpoint VS code
will execute every line of code

28
00:01:55.740 --> 00:01:59.850
until it hits our breakpoint.
From that point onward, we can

29
00:01:59.850 --> 00:02:04.860
execute our code line by line.
So let's add a breakpoint. And

30
00:02:04.860 --> 00:02:12.000
then start the application for
debugging. Alright, let me

31
00:02:12.000 --> 00:02:16.800
collapse this window. Good. So
in this new terminal window, we

32
00:02:16.800 --> 00:02:21.180
have a development server
listening on port 9000. So let's

33
00:02:21.180 --> 00:02:24.690
hold down the Command key on Mac
or Ctrl on Windows and click on

34
00:02:24.690 --> 00:02:28.320
this link. Alright, here's what
we get, the homepage of our

35
00:02:28.320 --> 00:02:31.680
project has gone because we have
registered a custom route that

36
00:02:31.680 --> 00:02:35.610
is playground slash Hello. So we
see the home page only the first

37
00:02:35.610 --> 00:02:40.230
time we run our Django project.
So let's go to playground slash

38
00:02:40.230 --> 00:02:44.880
Hello. Alright, our breakpoint
is activated, and we are on this

39
00:02:44.880 --> 00:02:49.080
line. On the left side. In the
variable section, you can see

40
00:02:49.080 --> 00:02:51.750
the local variables. So
currently, we have the request

41
00:02:51.750 --> 00:02:54.900
object, that is the request
object that we're receiving this

42
00:02:54.900 --> 00:02:59.640
function, so we can expand it
and inspect its various

43
00:02:59.670 --> 00:03:03.360
attributes. We don't want to do
that for now. So let's close

44
00:03:03.360 --> 00:03:07.680
this, we want to execute our
code line by line. So here we

45
00:03:07.680 --> 00:03:11.910
have a bunch of functions. The
first one is step over. And the

46
00:03:11.910 --> 00:03:15.120
shortcut for this is f 10. With
this, we can step over the

47
00:03:15.120 --> 00:03:17.190
current line. So if we press F
10,

48
00:03:18.870 --> 00:03:22.530
this line gets executed. And now
we are on the second line. Now

49
00:03:22.530 --> 00:03:26.370
look over here. In the list of
local variables, we have x and

50
00:03:26.400 --> 00:03:29.640
its value is one. This is very
useful when debugging

51
00:03:29.670 --> 00:03:33.570
applications. So if something is
not calculated properly, we can

52
00:03:33.570 --> 00:03:36.960
inspect it here. The most of the
time, you can see your local

53
00:03:36.960 --> 00:03:40.680
variables here. But if not, you
can always add them in the watch

54
00:03:40.680 --> 00:03:44.370
window. So before recording this
video I was practicing. That's

55
00:03:44.370 --> 00:03:48.960
why you see x here, let me
select this and delete it. So in

56
00:03:48.960 --> 00:03:51.870
your watch section, you're not
going to have any variables. To

57
00:03:51.870 --> 00:03:55.950
add one, we simply click on this
and then type the name of our

58
00:03:55.950 --> 00:04:00.750
variable. Okay. So this is how
we can step over various line so

59
00:04:00.780 --> 00:04:04.500
we can press f9 again, and
again. So this function is

60
00:04:04.500 --> 00:04:07.860
executed. And now back in the
browser, we see the final

61
00:04:07.860 --> 00:04:11.670
result. Now let's do something
more interesting. So back to our

62
00:04:11.670 --> 00:04:15.930
code. Let's close the terminal
window, so we have more space.

63
00:04:16.800 --> 00:04:21.810
I'm going to define a function
here called calculate. And here

64
00:04:21.810 --> 00:04:25.470
we're going to set x to one, y
two and return x, just some

65
00:04:25.470 --> 00:04:30.150
dummy code. Over here. We're
going to call the calculate

66
00:04:30.150 --> 00:04:35.550
function. Okay. Now save the
changes back in the browser.

67
00:04:35.940 --> 00:04:40.050
Let's refresh. So our view
function gets called. Now we are

68
00:04:40.050 --> 00:04:43.320
right here. Now this time,
instead of stepping over this

69
00:04:43.320 --> 00:04:46.560
line, we want to step into it.
Because if you step over this

70
00:04:46.560 --> 00:04:50.280
line, we're not going to see
what happened in the calculate

71
00:04:50.280 --> 00:04:54.570
function. We see the final
result. So we see x is set to

72
00:04:54.570 --> 00:04:58.680
one and we can verify that over
here as well. But sometimes we

73
00:04:58.680 --> 00:05:01.620
need to step into a function To
see what is happening there,

74
00:05:01.650 --> 00:05:05.940
maybe there is a bug inside the
calculate function. So let's

75
00:05:05.940 --> 00:05:12.660
restart the debugger. Okay, I'm
going to close this back in the

76
00:05:12.660 --> 00:05:19.140
browser. Let's refresh this
page. Okay, you're back to this

77
00:05:19.140 --> 00:05:22.650
line. Now this time, we're going
to step into this function. So

78
00:05:22.680 --> 00:05:26.040
look over here. This is the icon
for stepping into a function.

79
00:05:26.310 --> 00:05:30.510
And as you can see, the shortcut
is F 11. So if you press F 11,

80
00:05:32.010 --> 00:05:35.850
we go inside the calculate
function. Now, we can execute

81
00:05:35.850 --> 00:05:40.050
each line using f tab or step
over and see where something has

82
00:05:40.050 --> 00:05:43.230
gone wrong. Now, let's imagine
this calculate function is a

83
00:05:43.230 --> 00:05:46.230
large function with a lot of
code. At some point, we want to

84
00:05:46.230 --> 00:05:49.770
step out of it without having to
execute every line of code in

85
00:05:49.770 --> 00:05:53.190
this function. There's a
shortcut for this. So look over

86
00:05:53.190 --> 00:05:59.070
here. That is step out, the
shortcut is shift, and F 11. So

87
00:05:59.070 --> 00:06:03.600
if we press Shift, f 11, we get
back to the previous function.

88
00:06:04.230 --> 00:06:07.380
So this is how we can debug
Django applications in VS code.

89
00:06:07.740 --> 00:06:11.850
Now, once we are done, it's
always a good practice to remove

90
00:06:11.850 --> 00:06:14.850
these breakpoints, otherwise,
they get in the way. So as your

91
00:06:14.850 --> 00:06:18.330
debugger applications, you place
various breakpoints in different

92
00:06:18.330 --> 00:06:21.870
parts of your code, and you will
hit them all the time. So always

93
00:06:21.870 --> 00:06:24.990
remove your breakpoints once
you're done with that. So we're

94
00:06:24.990 --> 00:06:28.230
done with this debugging
session. Now, we can disconnect

95
00:06:28.230 --> 00:06:33.690
by pressing Shift, and f5 or
clicking on this icon. Alright,

96
00:06:33.990 --> 00:06:37.710
now one last step, before we
finish this lesson. On the top,

97
00:06:38.040 --> 00:06:42.270
under the run menu, look, we
have a command called run

98
00:06:42.390 --> 00:06:47.370
without debugging. The shortcut
on Mac is Ctrl, and f5. So if we

99
00:06:47.370 --> 00:06:50.820
use the shortcut, we can start
an application without having to

100
00:06:50.820 --> 00:06:54.840
run Python, manage that py run
server. Let me show you what I

101
00:06:54.840 --> 00:06:59.970
mean. So first, let's bring up
our terminal window. Here's the

102
00:06:59.970 --> 00:07:02.910
second terminal window that we
use for debugging. I'm going to

103
00:07:02.910 --> 00:07:06.900
delete this. Here's the first
terminal window where we started

104
00:07:06.900 --> 00:07:11.580
our application by running
Python manage.py run server. So

105
00:07:11.610 --> 00:07:16.110
let's press Ctrl C to stop the
server. Good. I'm also going to

106
00:07:16.110 --> 00:07:19.320
delete this terminal window.
Let's imagine we just opened

107
00:07:19.320 --> 00:07:22.800
this project in VS code, day
one. Now, to run this

108
00:07:22.800 --> 00:07:29.490
application without debugging,
we can press Ctrl f5. Now our

109
00:07:29.490 --> 00:07:33.600
application has started on port
9000. So we can go to this

110
00:07:33.600 --> 00:07:39.540
address, and then hit playground
slash Hello. So this is how we

111
00:07:39.540 --> 00:07:43.110
can run our application without
debugging. Just remember that if

112
00:07:43.110 --> 00:07:45.870
you use this command, your
breakpoints are not going to get

113
00:07:45.870 --> 00:07:48.750
hit. So if you want to debug
your applications, you should

114
00:07:48.750 --> 00:07:50.550
start it in the debug mode.
